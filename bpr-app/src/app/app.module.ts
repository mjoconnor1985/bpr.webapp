import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";

import { LoadingModule } from 'ngx-loading';

import { AppComponent } from './app.component';
import { BprDashboardComponent } from './bpr-dashboard/bpr-dashboard.component';
import {BprDashboardService} from "./bpr-dashboard/bpr-dashboard.service";

@NgModule({
  declarations: [
    AppComponent,
    BprDashboardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,

    LoadingModule
  ],
  providers: [BprDashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
