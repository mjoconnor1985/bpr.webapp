import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import 'rxjs/add/operator/map'

import  { BprMetric } from "./bprMetric";
import * as constants  from '../shared/constants'

@Injectable()
export class BprDashboardService {

  constructor(private http: HttpClient) { }

  getCortezProcessMetrics(date: string){
    return this.http.get<BprMetric[]>(constants.SIMA_API_HOSTNAME + '/dailybprmetrics/cz/processmetrics',{
      params: new HttpParams().set('date', date)
    }).map(data => data);
  }

  getGoldStrikeProcessMetrics(date:string){
    return this.http.get<BprMetric[]>(constants.SIMA_API_HOSTNAME + '/dailybprmetrics/gs/processmetrics',{
      params: new HttpParams().set('date', date)
    }).map(data => data);
  }

}
