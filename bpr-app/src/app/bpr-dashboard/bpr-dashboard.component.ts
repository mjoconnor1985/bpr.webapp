import {Component, OnInit} from '@angular/core';

import {BprMetric} from "./bprMetric";
import {BprDashboardService} from './bpr-dashboard.service';

@Component({
  selector: 'app-bpr-dashboard',
  templateUrl: './bpr-dashboard.component.html',
  styleUrls: ['./bpr-dashboard.component.css']
})
export class BprDashboardComponent implements OnInit {

  metrics: BprMetric[];
  selectedDate: string = '8/1/2017';
  loading: boolean = false;
  site: string = "cz";

  constructor(private bprDashboardService: BprDashboardService) {
  }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.loading = true;
    if(this.site == 'cz'){
      this.bprDashboardService.getCortezProcessMetrics(this.selectedDate)
        .subscribe(data => {
          this.metrics = data
          this.loading = false;
        }, err => {
          this.loading = false;
          this.metrics = [];
        });
    }
    else{
      this.bprDashboardService.getGoldStrikeProcessMetrics(this.selectedDate)
        .subscribe(data => {
          this.metrics = data
          this.loading = false;
        }, err => {
          this.loading = false;
          this.metrics = [];
        });
    }

  }
}
