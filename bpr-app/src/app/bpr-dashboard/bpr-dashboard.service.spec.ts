import { TestBed, inject } from '@angular/core/testing';

import { BprDashboardService } from './bpr-dashboard.service';

describe('BprDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BprDashboardService]
    });
  });

  it('should be created', inject([BprDashboardService], (service: BprDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
