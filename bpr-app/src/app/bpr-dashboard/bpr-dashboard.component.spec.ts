import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BprDashboardComponent } from './bpr-dashboard.component';

describe('BprDashboardComponent', () => {
  let component: BprDashboardComponent;
  let fixture: ComponentFixture<BprDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BprDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BprDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
