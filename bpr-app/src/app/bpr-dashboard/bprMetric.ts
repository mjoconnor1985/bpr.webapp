export interface BprMetric{
  name: string
  unit: number
  dailyValue: number
  weekToDateValue: number
  monthToDateValue: number
}
